package main

import (
    "fmt"
    "log"
    "net/http"
)

func handler(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintf(w, "Hi there, docker folks!\n")
}

func main() {
    http.HandleFunc("/", handler)
    fmt.Printf("Starting your server at :8080 port\n")
    log.Fatal(http.ListenAndServe(":8080", nil))
}
