#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <limits.h>
#include <string.h>
#include <stdlib.h>

int main() {
    char* name = getenv("MYNAME");
    if(name == NULL) {
        printf("MYNAME environment variable was not passed correctly\n");
        return 1;
    }
    printf("Hello %s!\n", name);

    uid_t uid = getuid();
    struct passwd *pwd;
    pwd = getpwuid(uid);

    printf("username: %s\n", pwd->pw_name);
    if(pwd != NULL && strcmp(pwd->pw_name, "intel") != 0) {
        printf("Incorrect username of process owner\n");
        return 1;
    }
    char cwd[PATH_MAX];
    if (getcwd(cwd, sizeof(cwd)) != NULL) {
        printf("%s\n", cwd);
    } else {
        printf("getcwd didn't work :(\n");
    }
    
    if(strcmp(cwd, "/home/intel/workspace") != 0) {
        printf("Incorrect working dir\n");
        return 1;
    }
    
    while(true) {
        sleep(1);
        printf("Well done!\n");
    }
}
