Task 1:

Let us begin first with the most basic operation i.e. mounting a data volume in one of our containers. We will be working with the **busybox** image to keep things simple.

We are going to use the -v [/VolumeName] as an option to mount a volume for our container. Let us launch a container as given below:

```
$ docker run -it -v /data --name container1 busybox
```

This will launch a container (named container1) in interactive mode and you will be at the prompt in the container.

Give the ls command as shown below:

```
$ docker run -it -v /data --name container1 busybox
/ # ls
bin   data  dev   etc   home  proc  root  sys   tmp   usr   var
/ #
```

Notice that a volume named **data** is visible now.

Let us a do a cd inside the data volume and create a file named file1.txt as shown below:

```
/ # cd data
/data # touch file1.txt
/data # ls
file1.txt
/data #
```

So what we have done so far is to mount a volume /data in the container. We navigated to that directory (/data) and then created a file in it.

Now, let us exit the container by typing exit and going back to the terminal.

```
/data # exit
$
```

Now, if we do a docker ps -a , we should see our container (container1) currently in the exited state as shown below:

```
CONTAINER ID IMAGE COMMAND CREATED STATUS PORTS NAMES
22ab6644ea6b busybox "/bin/sh" 3 minutes ago Exited (0) 23 seconds ago container1
```

Now, let us inspect the container and see what Docker did when we started this container.

ive the following command:

```
$ docker inspect container1
```

This will give out a JSON output and you should look for Mounts attribute in the output. A sample output from my machine is shown below:

```
"Mounts": [{
"Type": "volume",
"Name": "568258a7940182c5ac52f0637c60c1d1f81e9ec77f3e4694647b4879c2ff003c",
"Source": "/var/lib/docker/volumes/568258a7940182c5ac52f0637c60c1d1f81e9ec77f3e4694647b4879c2ff003c/_data",
"Destination": "/data",
"Driver": "local",
"Mode": "",
"RW": true,
"Propagation": ""
}],
```

This tells you that when you mounted a volume (**/data**), it has created a folder **/var/lib/….** for you, which is where it puts all the files, etc that you would have created in that volume. Note that we had created a **file1.txt** over there (we will come to that in a while).

Also notice that the **RW** mode is set to true i.e. Read and Write.

Now that the container is stopped i.e. exited, let us restart the container (**container1**) and see if our volume is still available and that **file1.txt** exists.

```
$ docker restart container1
container1$ docker attach container1/ # ls
bin   data  dev   etc   home  proc  root  sys   tmp   usr   var
/ # cd data
/data # ls
file1.txt
/data #
```

Exit the container and remove the container.

```
/data # exit$ docker rm container1
container1$ docker volume lsDRIVER              VOLUME NAME
local               568258a7940182c5ac52f0637c60c1d1f81e9ec77f3e4694647b4879c2ff003c
```

This shows to you that though you have removed the **container1**, the data volume is still present on the host. This is a dangling or ghost volume and could remain there on your machine consuming space. Do remember to clean up if you want. Alternatively, there is also a **-v** option while removing the container as shown in the help:

```
$ docker rm --helpUsage: docker rm [OPTIONS] CONTAINER [CONTAINER...]Remove one or more containers-f, --force=false Force the removal of a running container (uses SIGKILL)
 --help=false Print usage
 -l, --link=false Remove the specified link
 -v, --volumes=false Remove the volumes associated with the container
```

This will always remove your volumes when the container is also removed.

**Time for Task #2!**

**What happens if you launch another container with the same /data volume. Is the file still there or does each container get its own file system?**