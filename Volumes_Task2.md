The next step is to look at the same process of mounting a volume but this time we will mount an existing host folder in the Docker container. This is an interesting concept and is very useful if you are looking to do some development where you regularly modify a file in a folder outside and expect the container to take note or even sharing the volumes across different containers.
Create a folder on your host machine, i.e. called host_folder. Then, using touch, add a file, that will be a proof 



```
$ mkdir host_folder
$ cd host_folder
$ touch test_file
$ ls -al
total 0
drwxr-xr-x    2 root     root            23 Aug 31 07:08 .
drwxr-xr-x    1 root     root           137 Aug 31 07:03 ..
-rw-r--r--    1 root     root             0 Aug 31 07:08 test_file
```



To mount a host volume while launching a Docker container, we have to use the following format for volume **-v** :

```
-v HostFolder:ContainerVolumeName
```

So, let us start a busybox container as shown below:

```
$ docker run -it --name container1 -v /host_folder:/container_folder busybox
/ #
```

What we have done here is that we have mapped the host folder /host_folder to a volume /container_folder that will be mounted inside our container (container1).

Now, if we do a **ls** , we can see that the /container_folder has been mounted. Do a cd into that folder and a ls, and you should be able to see the folder contents of /host_folder on your machine.

```
/ # cd container_folder
/container_folder # ls
test_file
```

**Additional Note:** Optionally if you require, you can mount a single host file too as a data volume.

Start thinking now of how you would use host folders that have been mounted as data volumes. Assume you are doing development and have the Apache Web Server or any other Web Server running in the container. You could have started the container and mounted a host director that the Web Server can use. Now on your host machine, you could make changes using your tools and those would then get reflected directly into your Docker container.