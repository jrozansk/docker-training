## Data volume containers

We now come to the next part i.e. creating a Data volume container. This is very useful if you want to share data between containers or you want to use the data from non-persistent containers. The process is really two step:

1. You first create a Data volume container
2. Create another container and mount the volume from the container created in Step 1.

Let us see that in action:

We will first create a container (**container1**) and mount a volume inside of that:

```
$ docker run -it -v /data --name container1 busybox
```

Now, let us go into the /data volume and create two dummy files in it as shown below:

```
/ # cd data
/data # ls
/data # touch file1.txt
/data # touch file2.txt
/data # ls
file1.txt  file2.txt
```

Now jump out of the container using CTRL+PQ.

Now, if we do a **docker ps**, we should see our running container:

```
$ docker ps
CONTAINER ID IMAGE COMMAND CREATED STATUS PORTS
006f7ba16783 busybox "/bin/sh" 27 seconds ago Up 26 seconds
```

Now, if we execute a command on the running **container1** i.e. see the contents of our **/data** volume, you can see that the two files are present.

```
$  docker exec container1 ls /data
file1.txt
file2.txt
```

Great ! Now, let us launch another container (**container2**) but it will mount the data volume from container1 as given below:

```
$ docker run -it --volumes-from container1 --name container2 busybox
```

Notice above that we have launched it in interactive mode and have used a new parameters **— volumes-from** <**containername**> that we have specified. This tells **container2** to mount the volumes that **container1** mounted.

Now, if we do a **ls**, we can see that the data folder is present and if we do a **ls** inside of that, we can see our two files: **file1.txt** and **file2.txt**

```
/ # ls
bin dev home lib64 media opt root sbin tmp var
data etc lib linuxrc mnt proc run sys usr
/ # cd data
/data # ls
file1.txt file2.txt
/data #
```

You can launch multiple containers too , all using the same data volume from **container1**.

Again, jump out using CTRL+PQ and run another containers:

```
$ docker run -it --volumes-from container1 --name container3 busybox
$ docker run -it --volumes-from container1 --name container4 busybox
```